# Translation of accountwizard_maildir.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2011, 2013.
msgid ""
msgstr ""
"Project-Id-Version: accountwizard_maildir\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:16+0200\n"
"PO-Revision-Date: 2013-11-17 17:01+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: maildirwizard.es:20
msgid "Personal Settings"
msgstr "Личне поставке"

#. i18n: ectx: property (text), widget (QLabel, label)
#: maildirwizard.ui:19
#, kde-format
msgid "URL:"
msgstr "УРЛ:"
