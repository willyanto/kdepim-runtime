# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# g.sora <g.sora@tiscali.it>, 2011, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-09-18 00:17+0000\n"
"PO-Revision-Date: 2015-06-12 10:25+0200\n"
"Last-Translator: Giovanni Sora <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: vcardconfig.cpp:16
#, kde-format
msgctxt "Filedialog filter for *.vcf"
msgid "vCard Address Book File"
msgstr "File de adressario de vCard"

#: vcardresource.cpp:71
#, kde-format
msgid "Contact with uid '%1' not found."
msgstr "On non trovava contacto con uid '%1'."

#: vcardresource.cpp:164 vcardresource.cpp:184
#, kde-format
msgid "Unable to open vCard file '%1'."
msgstr "Incapace de aperir file de vCard '%1'."

#. i18n: ectx: label, entry (Path), group (General)
#: vcardresource.kcfg:10
#, kde-format
msgid "Path to vCard file."
msgstr "Percurso al file de vCard."

#. i18n: ectx: label, entry (DisplayName), group (General)
#: vcardresource.kcfg:14
#, kde-format
msgid "Display name."
msgstr "Nomine de monstrar."

#. i18n: ectx: label, entry (ReadOnly), group (General)
#: vcardresource.kcfg:18
#, kde-format
msgid "Do not change the actual backend data."
msgstr "Non modifica le datos actual de retro-administration."

#. i18n: ectx: label, entry (MonitorFile), group (General)
#: vcardresource.kcfg:22
#, kde-format
msgid "Monitor file for changes."
msgstr "Controla file pro modificationes."

#. i18n: ectx: property (text), widget (QLabel, label)
#: wizard/vcardwizard.ui:19
#, kde-format
msgid "Filename:"
msgstr "Nomine de file:"

#~ msgid "Select Address Book"
#~ msgstr "Selige adressario"
