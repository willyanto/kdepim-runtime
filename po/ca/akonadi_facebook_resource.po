# Translation of akonadi_facebook_resource.po to Catalan
# Copyright (C) 2017-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2017, 2020.
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdepim-runtime\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-01-27 03:07+0100\n"
"PO-Revision-Date: 2021-10-30 18:14+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: birthdaylistjob.cpp:78 birthdaylistjob.cpp:84
#, kde-format
msgid "Failed to retrieve birthday calendar"
msgstr "Ha fallat l'obtenció del calendari d'aniversaris"

#: birthdaylistjob.cpp:128
#, kde-format
msgid "Failed to parse birthday calendar"
msgstr "Ha fallat en analitzar el calendari d'aniversaris"

#. i18n: ectx: property (windowTitle), widget (QWidget, FacebookAgentSettingsWidget)
#: facebookagentsettingswidget.ui:14
#, kde-format
msgid "Configuration"
msgstr "Configuració"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: facebookagentsettingswidget.ui:26
#, kde-format
msgid "Account"
msgstr "Compte"

#. i18n: ectx: property (text), widget (QLabel, loginStatusLbl)
#: facebookagentsettingswidget.ui:32
#, kde-format
msgid "Login status: unknown"
msgstr "Estat de la connexió: desconegut"

#. i18n: ectx: property (text), widget (QPushButton, loginBtn)
#: facebookagentsettingswidget.ui:39
#, kde-format
msgid "Login with Facebook"
msgstr "Inici de la sessió amb Facebook"

#. i18n: ectx: property (text), widget (QPushButton, logoutBtn)
#: facebookagentsettingswidget.ui:46
#, kde-format
msgid "Logout"
msgstr "Sortida"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: facebookagentsettingswidget.ui:62
#, kde-format
msgid "Reminders"
msgstr "Recordatoris"

#. i18n: ectx: property (text), widget (QCheckBox, attendingReminderChkBox)
#: facebookagentsettingswidget.ui:68
#, kde-format
msgid "Show reminders for events I'm attending"
msgstr "Mostra recordatoris per als esdeveniments en què participo"

#. i18n: ectx: property (text), widget (QCheckBox, maybeAttendingReminderChkBox)
#: facebookagentsettingswidget.ui:78
#, kde-format
msgid "Show reminders for events I may be attending"
msgstr "Mostra recordatoris per als esdeveniments en què puc participar"

#. i18n: ectx: property (text), widget (QCheckBox, notAttendingReminderChkBox)
#: facebookagentsettingswidget.ui:88
#, kde-format
msgid "Show reminders for events I'm not attending"
msgstr "Mostra recordatoris per als esdeveniments en què no participo"

#. i18n: ectx: property (text), widget (QCheckBox, notRespondedReminderChkBox)
#: facebookagentsettingswidget.ui:98
#, kde-format
msgid "Show reminders for events I have not responded to"
msgstr "Mostra recordatoris per als esdeveniments que no he respost"

#. i18n: ectx: property (text), widget (QCheckBox, birthdayReminderChkBox)
#: facebookagentsettingswidget.ui:108
#, kde-format
msgid "Show reminders for friends' birthdays"
msgstr "Mostra recordatoris per a les dates de naixement dels amics"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: facebookagentsettingswidget.ui:138
#, kde-format
msgid "Show event reminders"
msgstr "Mostra recordatoris dels esdeveniments"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: facebookagentsettingswidget.ui:155
#, kde-format
msgid "hour(s) before"
msgstr "hora/es abans"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: facebookagentsettingswidget.ui:162
#, kde-format
msgid "Show birthday reminders"
msgstr "Mostra recordatoris de les dates de naixement"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: facebookagentsettingswidget.ui:172
#, kde-format
msgid "day(s) before"
msgstr "dia/es abans"

#: facebooksettingswidget.cpp:48
#, kde-format
msgid "Checking login status..."
msgstr "S'està verificant l'estat de l'accés..."

#: facebooksettingswidget.cpp:57
#, kde-format
msgid "Not logged in"
msgstr "No s'ha accedit al compte"

#: facebooksettingswidget.cpp:61
#, kde-format
msgid "Logged in as <b>%1</b>"
msgstr "Connectat com a <b>%1</b>"

#: listjob.cpp:87
#, kde-format
msgid "Invalid response from server: JSON parsing error"
msgstr "Resposta no vàlida del servidor: Error en analitzar el JSON"

#: resource.cpp:24 resource.cpp:71
#, kde-format
msgid "Facebook"
msgstr "Facebook"

#: resource.cpp:75
#, kde-format
msgid "Events I'm Attending"
msgstr "Esdeveniments en què participo"

#: resource.cpp:76
#, kde-format
msgid "Events I'm not Attending"
msgstr "Esdeveniments en què no participo"

#: resource.cpp:77
#, kde-format
msgid "Events I May Be Attending"
msgstr "Esdeveniments en què puc participar"

#: resource.cpp:78
#, kde-format
msgid "Events I have not Responded To"
msgstr "Esdeveniments que no he respost"

#: resource.cpp:79
#, kde-format
msgid "Friends' Birthdays"
msgstr "Dates de naixement dels amics"

#: tokenjobs.cpp:287 tokenjobs.cpp:299 tokenjobs.cpp:392 tokenjobs.cpp:445
#, kde-format
msgid "Failed to open KWallet"
msgstr "Ha fallat en obrir el KWallet"

#: tokenjobs.cpp:343
#, kde-format
msgid "Failed to obtain access token from Facebook"
msgstr "Ha fallat en obtenir el testimoni d'accés des de Facebook"
