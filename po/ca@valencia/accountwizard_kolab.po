# Translation of accountwizard_kolab.po to Catalan (Valencian)
# Copyright (C) 2015-2020 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2015, 2019.
# Josep M. Ferrer <txemaq@gmail.com>, 2015, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kdepim-runtime\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2020-11-28 19:30+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 2.0\n"

#: kolabwizard.es:10
msgid "Personal Settings"
msgstr "Configuració personal"

#: kolabwizard.es:11
msgid "Autoconfiguration"
msgstr "Configuració automàtica"

#: kolabwizard.es:76 kolabwizard.es:256
msgid "Probing IMAP server..."
msgstr "S'està provant un servidor IMAP..."

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxFreebusyEdit)
#. i18n: ectx: property (text), widget (QCheckBox, checkBoxLdapEdit)
#: kolabwizard.es:128 kolabwizard.es:129 kolabwizard2.ui:85 kolabwizard2.ui:163
#, kde-format
msgid "Create"
msgstr "Crea"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxImapEdit)
#. i18n: ectx: property (text), widget (QCheckBox, checkBoxSmtpEdit)
#: kolabwizard.es:176 kolabwizard.es:187 kolabwizard2.ui:55 kolabwizard2.ui:136
#, kde-format
msgid "Manual Edit"
msgstr "Edita manualment"

#: kolabwizard.es:213
msgid "Probing SMTP server..."
msgstr "S'està provant un servidor SMTP..."

#. i18n: ectx: property (text), widget (QLabel, nameLabel)
#: kolabwizard.ui:20
#, kde-format
msgid "&Name:"
msgstr "&Nom:"

#. i18n: ectx: property (text), widget (QLabel, emailLabel)
#: kolabwizard.ui:33
#, kde-format
msgid "&Email:"
msgstr "Correu &electrònic:"

#. i18n: ectx: property (text), widget (QLabel, passwordLabel)
#: kolabwizard.ui:46
#, kde-format
msgid "&Password:"
msgstr "&Contrasenya:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kolabwizard.ui:60
#, kde-format
msgid "Kolab Version:"
msgstr "Versió de Kolab:"

#. i18n: ectx: property (text), item, widget (QComboBox, versionComboBox)
#: kolabwizard.ui:74
#, kde-format
msgid "v2"
msgstr "v2"

#. i18n: ectx: property (text), item, widget (QComboBox, versionComboBox)
#: kolabwizard.ui:79
#, kde-format
msgid "v3"
msgstr "v3"

#. i18n: ectx: property (text), widget (QLabel, imapLabel)
#: kolabwizard2.ui:20
#, kde-format
msgid "IMAP:"
msgstr "IMAP:"

#. i18n: ectx: property (text), widget (QLabel, lineEditImapLabel)
#: kolabwizard2.ui:42
#, kde-format
msgid "IMAP Server Name:"
msgstr "Nom del servidor IMAP:"

#. i18n: ectx: property (text), widget (QLabel, labelFreebusy)
#: kolabwizard2.ui:94
#, kde-format
msgid "Freebusy:"
msgstr "Lliure/ocupat:"

#. i18n: ectx: property (text), widget (QLabel, labelSmtp)
#: kolabwizard2.ui:101
#, kde-format
msgid "SMTP:"
msgstr "SMTP:"

#. i18n: ectx: property (text), widget (QLabel, lineEditSmtpLabel)
#: kolabwizard2.ui:123
#, kde-format
msgid "SMTP Server Name:"
msgstr "Nom del servidor SMTP:"

#. i18n: ectx: property (text), widget (QLabel, labelLdap)
#: kolabwizard2.ui:172
#, kde-format
msgid "LDAP:"
msgstr "LDAP:"
