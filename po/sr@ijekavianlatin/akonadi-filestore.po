# Translation of akonadi-filestore.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: akonadi-filestore\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:16+0200\n"
"PO-Revision-Date: 2011-08-20 23:55+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: abstractlocalstore.cpp:414 abstractlocalstore.cpp:445
#: abstractlocalstore.cpp:477 abstractlocalstore.cpp:503
#: abstractlocalstore.cpp:534 abstractlocalstore.cpp:567
#: abstractlocalstore.cpp:595 abstractlocalstore.cpp:629
#: abstractlocalstore.cpp:666 abstractlocalstore.cpp:700
#: abstractlocalstore.cpp:734 abstractlocalstore.cpp:786
#, kde-format
msgctxt "@info:status"
msgid "Configured storage location is empty"
msgstr "Podešena skladišna lokacija je prazna."

#: abstractlocalstore.cpp:419 abstractlocalstore.cpp:451
#: abstractlocalstore.cpp:482 abstractlocalstore.cpp:508
#: abstractlocalstore.cpp:541 abstractlocalstore.cpp:572
#: abstractlocalstore.cpp:636 abstractlocalstore.cpp:742
#, kde-format
msgctxt "@info:status"
msgid "Given folder name is empty"
msgstr "Dato ime fascikle je prazno."

#: abstractlocalstore.cpp:424 abstractlocalstore.cpp:546
#, kde-format
msgctxt "@info:status"
msgid "Access control prohibits folder creation in folder %1"
msgstr "Kontrola pristupa zabranjuje stvaranje fascikle u fascikli %1."

#: abstractlocalstore.cpp:456
#, kde-format
msgctxt "@info:status"
msgid "Access control prohibits folder deletion in folder %1"
msgstr "Kontrola pristupa zabranjuje brisanje fascikle u fascikli %1."

#: abstractlocalstore.cpp:513
#, kde-format
msgctxt "@info:status"
msgid "Access control prohibits folder modification in folder %1"
msgstr "Kontrola pristupa zabranjuje menjanje fascikle u fascikli %1."

#: abstractlocalstore.cpp:601 abstractlocalstore.cpp:672
#: abstractlocalstore.cpp:706 abstractlocalstore.cpp:763
#, kde-format
msgctxt "@info:status"
msgid "Given item identifier is empty"
msgstr "Dati identifikator stavke je prazan."

#: abstractlocalstore.cpp:643 abstractlocalstore.cpp:749
#, kde-format
msgctxt "@info:status"
msgid "Access control prohibits item creation in folder %1"
msgstr "Kontrola pristupa zabranjuje stvaranje stavke u fascikli %1."

#: abstractlocalstore.cpp:678
#, kde-format
msgctxt "@info:status"
msgid "Access control prohibits item modification in folder %1"
msgstr "Kontrola pristupa zabranjuje menjanje stavke u fascikli %1."

#: abstractlocalstore.cpp:712 abstractlocalstore.cpp:756
#, kde-format
msgctxt "@info:status"
msgid "Access control prohibits item deletion in folder %1"
msgstr "Kontrola pristupa zabranjuje brisanje stavke u fascikli %1."
