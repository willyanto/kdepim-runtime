# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Xosé <xosecalvo@gmail.com>, 2010.
# Marce Villarino <mvillarino@kde-espana.org>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-15 02:21+0200\n"
"PO-Revision-Date: 2013-11-16 23:30+0100\n"
"Last-Translator: Marce Villarino <mvillarino@kde-espana.org>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: maildirwizard.es:7
msgid "Personal Settings"
msgstr "Configuración persoal"

#. i18n: ectx: property (text), widget (QLabel, label)
#: maildirwizard.ui:19
#, kde-format
msgid "URL:"
msgstr "URL:"
